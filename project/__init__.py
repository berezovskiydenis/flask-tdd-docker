import os

from flask import Flask
from flask_admin import Admin
from flask_sqlalchemy import SQLAlchemy

# DB instance
db = SQLAlchemy()

# Init admin
admin = Admin(template_mode="bootstrap3")


def create_app(script_info=None):
    # Application instance
    app = Flask(__name__)

    # Set config from file
    app_settings = os.getenv("APP_SETTINGS")
    app.config.from_object(app_settings)
    # Init DB
    db.init_app(app)
    # Init admin
    if os.getenv("FLASK_ENV") == "development":
        admin.init_app(app)

    # Registester API
    from project.api import api
    api.init_app(app)

    # Shell context for Flask CLI
    @app.shell_context_processor
    def ctx():
        return {"app": app, "db": db}

    return app
