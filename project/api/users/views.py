from flask import request
from flask_restx import Namespace, Resource, fields

from project.api.users.crud import (add_user, delete_user, get_all_users,
                                    get_user_by_email, get_user_by_id,
                                    update_user)


users_namespace = Namespace("users")

user = users_namespace.model(
    "User", {
        "id": fields.Integer(readOnly=True),
        "username": fields.String(required=True),
        "email": fields.String(required=True),
        "create_date": fields.DateTime
    }
)


class UserList(Resource):
    @users_namespace.expect(user, validate=True)
    @users_namespace.response(201, "<user_email> was added!")
    @users_namespace.response(400, "That email already exists")
    def post(self):
        post_data = request.get_json()
        username = post_data.get("username")
        email = post_data.get("email")

        response_object = {}

        user = get_user_by_email(email)

        if user:
            response_object["message"] = "That email already exists"
            return response_object, 400

        add_user(username, email)
        response_object["message"] = "{} was added!".format(email)
        return response_object, 201

    @users_namespace.marshal_with(user, as_list=True)
    def get(self):
        """Return all users."""
        return get_all_users(), 200


class Users(Resource):
    @users_namespace.marshal_with(user)
    @users_namespace.response(200, "Success")
    @users_namespace.response(404, "User <user_id> does not exist")
    def get(self, user_id):
        """Rerurns a single user."""
        user = get_user_by_id(user_id)
        if not user:
            return users_namespace.abort(
                404,
                "User {} does not exist".format(user_id)
            )
        return user, 200

    @users_namespace.response(200, "<user_id> was removed")
    @users_namespace.response(404, "User <user_id> does not exists")
    def delete(self, user_id):
        """Updates a user."""
        response_object = {}
        user = get_user_by_id(user_id)

        if not user:
            users_namespace.abort(
                404,
                "User {} does not exists".format(user_id)
            )

        delete_user(user)

        response_object["message"] = "{} was removed".format(user.email)
        return response_object, 200

    @users_namespace.expect(user, validate=True)
    @users_namespace.response(200, "<user_id> was updated")
    @users_namespace.response(404, "User <user_id> does not exist")
    def put(self, user_id):
        post_data = request.get_json()
        username = post_data.get("username")
        email = post_data.get("email")
        response_object = {}

        user = get_user_by_id(user_id)
        if not user:
            users_namespace.abort(404, f"User {user_id} does not exist")
        update_user(user, username, email)
        response_object["message"] = f"{user.id} was updated!"
        return response_object, 200


users_namespace.add_resource(UserList, "")
users_namespace.add_resource(Users, "/<int:user_id>")
